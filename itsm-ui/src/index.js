import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'
import * as Sentry from '@sentry/browser'
import * as serviceWorker from './serviceWorker'
import './styles/tailwind.css'
import { ApolloProvider } from 'react-apollo'
import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'

Sentry.init({
  dsn: 'https://6c31ee1d34f14ab6bb45102269af9d45@sentry.io/4447182',
})

const httpLink = createHttpLink({
  uri: 'http://localhost:4000',
})

const client = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache(),
})

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById('root'),
)
serviceWorker.unregister()
