import React from 'react'
import { render } from '@testing-library/react'
import App from '../components/App'

test('renders learn react link', () => {
  const { getByText } = render(<App />)
  const linkElement = getByText(/EMAIL ADDRESS/i)
  const passwordElement = getByText(/PASSWORD/i)
  const signInElement = getByText(/Sign In/i)
  expect(linkElement).toBeInTheDocument()
  expect(signInElement).toBeInTheDocument()
  expect(passwordElement).toBeInTheDocument()
})
